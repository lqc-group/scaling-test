#!/bin/bash

defaultSize=128 # define the size of meshes on one core
memSize=1 # define the memory needed for one core
strongScale=16 # define the size of
declare -a length=(1 2 4 8 16  32   64 ) #16 32 64 128 256 512 1024)
declare -a core=(1 4 16 64 256 1024 4096) #16 32 64 128 256 512 1024)
declare -a  ppn=(1 4 16 64 64  64   64  ) #8  8  8  8   8   8   8)
declare -a node=(1 1 1  1  4   16   64 ) #2  4  8  16  32  64  128)
fenicsFile="poisson.py"
pbsFile="nothing.pbs"
executeCMD="echo ${pbsFile}"
getWallTimeCMD="grep 'Wall' log.txt | rev | cut -d' ' -f1 | rev"
getProcessTimeCMD="grep 'Process' log.txt | rev | cut -d' ' -f1 | rev"
#-- In the ideal case, everything you need to change should be above this line --#

if [ -z "$1" ]; then
    echo "You must pass either --create or --run option"
fi

let count=(${#core[@]}-1)

while [ -n "$1" ]; do
    case "$1" in
        --create) echo "Creating folder"
            echo "Folder for weak_scaling"
            mkdir weak_scaling
            cd weak_scaling
            for index in `seq 0 1 ${count}`; do
                let mesh=$(echo "${defaultSize}*${length[index]}" | bc)
                let mem=$(echo "${core[index]}*${memSize}" | bc -l)
                echo ${core[index]} ${node[index]} ${mesh}
                mkdir core_${core[index]}
                cd core_${core[index]}
                cp ../../${fenicsFile} .
                cp ../../${pbsFile} .
                sed -i "s/MESHSIZE/${mesh}/" ${fenicsFile}
                sed -i "s/MESHSIZE/${mesh}/" ${pbsFile}
                sed -i "s/CORE/${core[index]}/" ${pbsFile}
                sed -i "s/NODE/${node[index]}/" ${pbsFile}
                sed -i "s/PPN/${ppn[index]}/" ${pbsFile}
                sed -i "s/MEM/${mem}GB/" ${pbsFile}
                cd ..
            done

            cd ..

            echo "Folder for strong_scaling"
            mkdir strong_scaling
            cd  strong_scaling
            for index in `seq 0 1 ${count}`; do
                let mesh=$(echo "${defaultSize}*${strongScale}" | bc)
                let mem=$(echo "${memSize}*${strongScale}*${strongScale}" | bc -l)
                echo ${core[index]} ${node[index]} ${mesh}
                mkdir core_${core[index]}
                cd core_${core[index]}
                cp ../../${fenicsFile} .
                cp ../../${pbsFile} .
                sed -i "s/MESHSIZE/${mesh}/" ${fenicsFile}
                sed -i "s/MESHSIZE/${mesh}/" ${pbsFile}
                sed -i "s/CORE/${core[index]}/" ${pbsFile}
                sed -i "s/NODE/${node[index]}/" ${pbsFile}
                sed -i "s/PPN/${ppn[index]}/" ${pbsFile}
                sed -i "s/MEM/${mem}GB/" ${pbsFile}
                cd ..
            done
            cd ..

            ;;

        --run) echo "Running jobs"
            cd weak_scaling
            for index in `seq 0 1 ${count}`; do
                let mesh=$(echo "${defaultSize}*${length[index]}" | bc)
                echo ${core[index]} ${node[index]} ${mesh}
                cd core_${core[index]}
                eval ${executeCMD}
                cd ..
            done

            cd ..

            echo "Folder for strong_scaling"
            cd  strong_scaling
            for index in `seq 0 1 ${count}`; do
                echo ${core[index]} ${node[index]}
                cd core_${core[index]}
                eval ${executeCMD}
                cd ..
            done
            cd ..
            ;;

        --analyze) echo "Obtaining data from output"
            cd weak_scaling
            touch weak.log
            echo "Node,Core,Wall,Process\n" >> weak.log
            for index in `seq 0 1 ${count}`; do
                let mesh=$(echo "${defaultSize}*${length[index]}" | bc)
                echo ${core[index]} ${node[index]} ${mesh}
                cd core_${core[index]}
                wallTime=$(eval ${getWallTimeCMD})
                processTime=$(eval ${getProcessTimeCMD})
                cd ..
                echo "${node[index]},${core[index]},${wallTime},${processTime}\n" >> weak.log
            done

            cd ..
            cd  strong_scaling
            echo "Node,Core,Wall,Process\n" >> strong.log
            touch strong.log
            for index in `seq 0 1 ${count}`; do
                echo ${core[index]} ${node[index]}
                cd core_${core[index]}
                wallTime=$(eval ${getWallTimeCMD})
                processTime=$(eval ${getProcessTimeCMD})
                cd ..
                echo "${node[index]},${core[index]},${wallTime},${processTime}\n" >> strong.log
            done
            cd ..
            ;;
        *) echo "Option must be either --create or --run" ;;

        esac

        shift
done

