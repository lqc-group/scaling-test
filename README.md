# Scaling test
## Purpose
1. To create the folder structure of weak and strong scaling test
2. To submit the jobs of testing

## Preparation
1. Install FEniCS
    - `module load anaconda`, the name probably is different on different systems
    - `conda create -n fenicsproject -c conda-forge fenics`, install FEniCS through anaconda 
2. Download the code and script to be tested
    - `git clone https://gitlab.com/lqc-group/scaling-test.git`

## Verify FEniCS is working fine
1. `module load anaconda`, to enable the conda commands
2. `conda activate fenicsproject`, to activate the fenics environment, on some system if this failed, you may need to run `conda init bash` first
3. `python demo_poisson.py`, to execute the FEniCS python script

## Make sure you check update the pbs file to accomodate the differences across different system, such as the path, python name, etc

## Run the test
1. Edit the first few lines of script.sh if necessary.
2. `./script.sh --create`, to generate the folder structure
3. Go into several example folder to check whether there is anything wrong in the pbs and python file
4. `./script.sh --run`, to submit the jobs
